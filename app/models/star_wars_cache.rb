class StarWarsCache < ApplicationRecord
  validates :path, uniqueness: true
  validates :path, :payload, presence: true
  def self.get(path)
    record = StarWarsCache.find_by(path: path)
    if record
      record.update_payload if record.stale?
      return record
    else
      return StarWarsCache.create(path: path, payload: StarWarsCache.fetch(path))
    end
  end

  def stale?
    return self.updated_at < 3.days.ago
  end

  def update_payload
    return self.update(payload: StarWarsCache.fetch(self.path))
  end

  def self.fetch(path)
    response = HTTParty.get("https://swapi.co/api#{path}", format: :json)
    return JSON.parse(response.body)
  end
end
