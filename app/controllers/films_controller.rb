class FilmsController < ApplicationController
  def index
    @response = StarWarsCache.get(request.original_fullpath).payload
  end

  def show
    @response = StarWarsCache.get(request.original_fullpath).payload

    respond_to do |format|
      format.html
      format.js
    end
  end
end
