class PeopleController < ApplicationController
  def index
    @response = StarWarsCache.get(request.original_fullpath).payload
  end
  
  def show
    @response = StarWarsCache.get("/people/#{params[:id]}").payload

    respond_to do |format|
      format.html
      format.js
    end
  end
end
