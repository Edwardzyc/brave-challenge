module ApplicationHelper
  def get_entity_number(url)
    return url.split("/").last
  end

  def strip_api_prefix(url)
    return url.gsub("https://swapi.co/api", "")
  end
end
