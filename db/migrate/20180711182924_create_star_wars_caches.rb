class CreateStarWarsCaches < ActiveRecord::Migration[5.2]
  def change
    create_table :star_wars_caches do |t|
      t.string :path
      t.json :payload
      t.timestamps
    end
    add_index :star_wars_caches, :path, unique: true
  end
end
