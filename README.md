# Brave Technical Challenge

**Please note:** This was built with ruby 2.5.0

## How to Setup

**Step 1:** Install the gems with `bundle install`

**Step 2:** Create the databases and migrate them `rake db:create` / `rake db:migrate`

**Step 3:** Run the server `rails s`
