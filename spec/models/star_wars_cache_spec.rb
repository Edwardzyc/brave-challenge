require 'rails_helper'

RSpec.describe StarWarsCache, type: :model do
  describe 'fetch()' do
    it 'calls HTTParty with the correct parameters and returns correct json' do
      returnObj = OpenStruct.new
      returnObj.body = {'hello' => 'world'}.to_json
      expect(HTTParty).to receive(:get).with('https://swapi.co/api/test-path', format: :json)
      .and_return(returnObj)
      expect(StarWarsCache.fetch('/test-path')).to eq({'hello' => 'world'})
    end 
  end

  describe 'get()' do
    context 'record in cache' do

      context 'record is stale' do
        it 'calls update_payload while returning record' do
          path = '/films'
          model = StarWarsCache.new(path: path)
          expect(StarWarsCache).to receive(:find_by).with(path: path)
          .and_return(model)
          expect(model).to receive(:stale?).and_return(true)
          expect(model).to receive(:update_payload)
          expect(StarWarsCache.get(path)).to eq(model)
        end
      end

      context 'record is not stale' do
        it 'returns the record' do
          path = '/films'
          model = StarWarsCache.new(path: path)
          expect(StarWarsCache).to receive(:find_by).with(path: path)
          .and_return(model)
          expect(model).to receive(:stale?).and_return(false)
          expect(model).to_not receive(:update_payload)
          expect(StarWarsCache.get(path)).to eq(model)
        end
      end
    end

    context 'record is not in cache' do
      it 'creates and returns the record' do
        path = '/films'
        model = StarWarsCache.create(path: path)
        hello_world_json = { 'hello' => 'world' }.to_json
        expect(StarWarsCache).to receive(:find_by).with(path: path)
        .and_return(nil)
        expect(StarWarsCache).to receive(:fetch).with(path).and_return(hello_world_json)
        expect(StarWarsCache).to receive(:create).with(path: path, payload: hello_world_json)
        .and_return(model)
        expect(StarWarsCache.get(path)).to eq(model)
      end
    end
  end

  describe 'update_payload()' do
    it 'updates payload to the return value of fetch' do
      hello_world_json = {'hello' => 'world'}.to_json
      model_path = '/random-path'
      expect(StarWarsCache).to receive(:fetch).with(model_path)
      .and_return(hello_world_json)
      model = StarWarsCache.new(path: model_path)
      expect(model).to receive(:update).with(payload: hello_world_json)
      model.update_payload
    end
  end
  describe 'stale()' do
    it 'returns true if updated_at is older than 3 days' do
      model = StarWarsCache.new
      model.updated_at = 4.days.ago
      expect(model.stale?).to eq(true)
    end

    it 'returns false if updated_at is younger than 3 days' do
      model = StarWarsCache.new
      model.updated_at = 2.days.ago
      expect(model.stale?).to eq(false)
    end
  end
end
